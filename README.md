# Webpack Simple Example

## 1 - Overview

This repo is a ready-to-run example of a webpack built web page.

As can be seen in [`package.json`](package.json) it uses:
+ [Webpack](https://webpack.js.org/concepts/) v4
+ [Babel](https://babeljs.io/docs/en/) v7 in the webpack config file [`webpack.config.babel.js`](webpack.config.babel.js) itself, hence the name **babel** in it.

The webpack config file allows to build a single `.html` file.  
To revert to the more usual case of a separate `index.js` file, comment the plugin `HtmlWebpackInlineSourcePlugin`.

## 2 - Install

From terminal, install the node packages listed in [`package.json`](package.json):

```bash
# from repo top folder
$ yarn install
```

## 3 - Development

To launch a development server:

```bash
# from repo top folder
$ yarn dev
# visit localhost:9001
```

A source code update will trigger a page refresh.


## 4 - Production

To build the production files in folder `dist/`:

```bash
# from repo top folder
$ yarn build
```

